class MainTransactionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_main_transaction, only: [:show, :edit, :update, :destroy]

  # GET /main_transactions
  # GET /main_transactions.json
  def index
    respond_to do |format|
      format.html
      format.json { render json: MainTransactionDatatable.new(view_context) }
    end
  end

  # GET /main_transactions/1
  # GET /main_transactions/1.json
  def show
  end

  # GET /main_transactions/new
  def new
    @main_transaction = MainTransaction.new
  end

  # GET /main_transactions/1/edit
  def edit
  end

  # POST /main_transactions
  # POST /main_transactions.json
  def create
    @main_transaction = MainTransaction.new(main_transaction_params)

    respond_to do |format|
      if @main_transaction.save
        format.html { redirect_to @main_transaction, notice: 'Main transaction was successfully created.' }
        format.json { render :show, status: :created, location: @main_transaction }
      else
        format.html { render :new }
        format.json { render json: @main_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /main_transactions/1
  # PATCH/PUT /main_transactions/1.json
  def update
    respond_to do |format|
      if @main_transaction.update(main_transaction_params)
        format.html { redirect_to @main_transaction, notice: 'Main transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @main_transaction }
      else
        format.html { render :edit }
        format.json { render json: @main_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /main_transactions/1
  # DELETE /main_transactions/1.json
  def destroy
    @main_transaction.destroy
    respond_to do |format|
      format.html { redirect_to main_transactions_url, notice: 'Main transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_main_transaction
      @main_transaction = MainTransaction.includes(transaction_items: {item: {item_prices: :unit}}).references(transaction_items: {item: {item_prices: :unit}}).find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def main_transaction_params
      params.require(:main_transaction).permit(:number, :transaction_date, :transaction_type_id, transaction_items_attributes: [:id, :_destroy, :item_id, :unit_id, :qty, :price])
    end
end
