class ItemDatatable < AjaxDatatablesRails::Base

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      name: { source: 'Item.name' },
      category_title: { source: 'Category.name' },
      item_prices: { source: 'ItemPrice.price' },
      is_active: { source: 'Item.is_active' }
    }
  end

  def data
    records.map do |item|
      {
        name:           link_to(item.name, item),
        category_title: item.category_title,
        item_prices:    item.ordered_item_prices.collect(&:title).join('<br>'.html_safe),
        is_active:      item.is_active ? fa_icon('check') : fa_icon('times'),
        action:         action_links(item),
        DT_RowId:       dom_id(item)
      }
    end
  end

  private

  def_delegators :@view, :link_to, :fa_icon, :item_path, :edit_item_path, :dom_id, :concat, :capture

  def get_raw_records
    Item.eager_load(category: :parent, item_prices: :unit).distinct
  end

  # ==== Insert 'presenter'-like methods below if necessary
  def action_links(item)
    capture do
      concat link_to(fa_icon('info-circle'), item_path(item), class: 'text-success')
      concat "&nbsp;".html_safe
      concat link_to(fa_icon('pencil'), edit_item_path(item), class: 'text-info')
      concat "&nbsp;".html_safe
      concat link_to(fa_icon('trash'), item_path(item), method: :delete, data: {confirm: 'Anda yakin?'}, class: 'text-danger')
    end
  end
end
