jQuery ($) ->
  $('#sales_filter_form').submit (e) ->
    e.preventDefault()
    table = $('.datatable').DataTable()
    url_main = table.ajax.url().split('?')[0]
    url_query = table.ajax.url().split('?')[1]
    searchParams = new URLSearchParams url_query
    searchParams.delete 'date_range'
    searchParams.set 'date_range', $('#date_range').val()
    new_url = url_main + '?' + searchParams.toString()
    table.page.len(-1).ajax.url(new_url).load()
