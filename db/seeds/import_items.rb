# insert items from import_items
ImportItem.all.each do |import_item|
  #find or create reference data
  unit = Unit.find_or_create_by(name: import_item.unit) do |unit|
    unit.qty = 1
    unit.is_base = true
  end
  parent_category = Category.find_or_create_by(name: import_item.category)
  item_category = Category.find_or_create_by(name: import_item.subcat1, parent: parent_category)
  #find or create item
  item = Item.find_or_create_by(name: import_item.name, abbr: import_item.itemcode, autoid: import_item.autoid, is_active: import_item.is_active) do |item|
    item.category = item_category
  end
  if item
    item.with_lock do
      ItemPrice.find_or_create_by(item: item, unit: unit) do |item_price|
        item_price.position = 1
        item_price.min_purchase = 1
        item_price.price = import_item.saleprice
        item_price.buy_price = import_item.buyprice
      end
      # ItemStock.find_or_create_by(item: item, unit: unit) do |item_stock|
      #   item_stock.position = 1
      #   item_stock.qty = import_item.curr_qty
      # end
      # remove imported data
      import_item.destroy
    end
  end
end
puts 'finished moving import items data into items table'
