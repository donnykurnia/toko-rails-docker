class CreateTransactionTypes < ActiveRecord::Migration
  def change
    create_table :transaction_types, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.string :name
      t.integer :sign

      t.timestamps null: false
    end
  end
end
