class SalesReportsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_sales_item_stocks, only: [:show]

  # GET /sales_reports
  # GET /sales_reports.json
  def index
    respond_to do |format|
      format.html
      format.json { render json: SalesReportDatatable.new(view_context, date_range: params[:date_range]) }
    end
  end

  # GET /sales_reports/1
  # GET /sales_reports/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_item_stocks
      @sales_item_stocks = DailyItemStock.includes(:unit, item: :category).references(:unit, item: :category).where(transaction_date: params[:id]).where('total_qty_out > 0')
    end
end
