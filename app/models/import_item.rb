# == Schema Information
#
# Table name: import_items
#
#  id         :uuid             not null, primary key
#  autoid     :string
#  name       :string
#  buyprice   :decimal(15, 2)
#  multi_s_pr :string
#  saleprice  :decimal(15, 2)
#  unit       :string
#  curr_qty   :float
#  svalue     :decimal(15, 2)
#  cvalue     :decimal(15, 2)
#  itemcode   :string
#  createdate :date
#  re_or_qty  :float
#  order_qty  :float
#  order_tol  :float
#  order_op   :string
#  category   :string
#  subcat1    :string
#  operator   :string
#  lastsaled  :date
#  lastinpric :decimal(15, 2)
#  lastin_qty :float
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  is_active  :boolean          default(TRUE)
#
# Indexes
#
#  index_import_items_on_is_active  (is_active)
#

class ImportItem < ActiveRecord::Base
end
