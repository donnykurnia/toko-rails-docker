class AddIsActiveToImportItem < ActiveRecord::Migration
  def change
    add_column :import_items, :is_active, :boolean, default: true
    add_index :import_items, :is_active
  end
end
