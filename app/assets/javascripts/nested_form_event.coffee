$(document).on 'nested:fieldAdded', (event) ->
  # this field was just inserted into your form
  field = event.field
  # init select2
  field.find('select.select2').select2()
  return

calculate_item_total = (scope) ->
  qty = parseFloat(scope.find('.transaction-item-qty').val()) or 0
  price = parseFloat(scope.find('.transaction-item-price').val()) or 0
  return qty * price

calculate_total_qty = ($) ->
  total_qty = 0
  $('.transaction-item-qty:visible').each (index, qty) ->
    total_qty += parseFloat($(qty).val()) or 0
    return
  return total_qty

calculate_total_total_price = ($) ->
  total_total_price = 0
  $('.transaction-item-total-price:visible').each (index, total_price) ->
    total_total_price += parseFloat($(total_price).val()) or 0
    return
  return total_total_price

calculate_main_transaction_total = ($) ->
  $('.main-transaction-total-qty').text calculate_total_qty($)
  $('.main-transaction-total-total-price').text calculate_total_total_price($)
  return

init_select2_ajax = (scope) ->
  # init item-select2-ajax
  scope.find('.items-select2-ajax').select2
    ajax:
      dataType: 'json'
      delay: 250
      data: (params) ->
        {
          q: params.term
          page: params.page
        }
      processResults: (data, params) ->
        params.page = params.page or 1
        {
          results: data.items
          pagination: more: params.page * 30 < data.total_count
        }
      cache: true
    escapeMarkup: (markup) ->
      markup
    minimumInputLength: 1
    templateResult: (item) ->
      if item.loading
       return item.text
      markup = '<p>' + item.title + '</p>'
      if item.item_prices.length > 0
        markup += '<ul>'
        for item_price in item.item_prices
          markup += '<li>' + item_price.title + '</li>'
        markup += '</ul>'
      return markup
    templateSelection: (item) ->
      if item.item_prices && item.item_prices.length > 0
        item_prince = item.item_prices[0]
        # set price
        price_field = scope.find('.transaction-item-price')
        if $('.main_transaction-transaction_type:checked').data('sign') == 1
          # transaksi pembelian
          price_field.val item.item_prices[0].buy_price
        else # transaksi penjualan
          price_field.val item.item_prices[0].price
        # trigger update
        price_field.trigger 'keyup'
        # set unit
        scope.find('.transaction-item-unit').removeAttr 'checked'
        scope.find('.transaction-item-unit[value="'+item_prince.unit_id+'"]').attr 'checked', 'checked'
        # set item-name in edit
        markup = '<p>' + item.title + '</p>'
        markup += '<ul>'
        for item_price in item.item_prices
          markup += '<li>' + item_price.title + '</li>'
        markup += '</ul>'
        scope.find('.item-name').html markup
      return item.title || item.name || item.text
  return

$(document).on 'nested:fieldAdded:transaction_items', (event) ->
  # this field was just inserted into your form
  field = event.field
  # auto calculation
  # bind keyup event
  field.find('.transaction-item-qty,.transaction-item-price').bind 'keyup mouseup mousewheel blur', (e) ->
    field.find('.transaction-item-total-price').val calculate_item_total(field)
    calculate_main_transaction_total($)
    return
  # init item-select2-ajax
  init_select2_ajax(field)
  return

$(document).on 'nested:fieldRemoved:transaction_items', (event) ->
  # this field was just inserted into your form
  field = event.field
  # auto calculation
  calculate_main_transaction_total($)
  return

jQuery ($) ->
  $('#transaction_items .fields:visible').each (index, fields_row) ->
    field = $(fields_row)
    # auto calculation
    field.find('.transaction-item-total-price').val calculate_item_total(field)
    calculate_main_transaction_total($)
    # bind keyup event
    field.find('.transaction-item-qty,.transaction-item-price').bind 'keyup mouseup mousewheel blur', (e) ->
      field.find('.transaction-item-total-price').val calculate_item_total(field)
      calculate_main_transaction_total($)
      return
    # init item-select2-ajax
    init_select2_ajax(field)
    return
  return
