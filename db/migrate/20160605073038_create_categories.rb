class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.uuid :parent_id
      t.string :name

      t.timestamps null: false
    end
    add_index :categories, :parent_id
    add_index :categories, :name
    add_index :categories, :created_at
  end
end
