CSV.foreach(Rails.root.join('db', 'csv', "supplier.csv"), {headers: true}) do |row|
  Customer.find_or_create_by(name: row["name"]) do |customer|
    customer.telephone = row["telephone"]
    customer.address = row["address"]
    customer.tipe = 'supplier'
  end
end
CSV.foreach(Rails.root.join('db', 'csv', "pelanggan.csv"), {headers: true}) do |row|
  Customer.find_or_create_by(name: row["name"]) do |customer|
    customer.tipe = 'pelanggan'
  end
end
puts 'finished seeding supplier dan pelanggan data'
