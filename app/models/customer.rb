# == Schema Information
#
# Table name: customers
#
#  id         :uuid             not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  telephone  :string
#  address    :text
#  tipe       :string
#
# Indexes
#
#  index_customers_on_created_at  (created_at)
#  index_customers_on_name        (name)
#

class Customer < ActiveRecord::Base
  # associations
  has_many :main_transactions, inverse_of: :customer, dependent: :restrict_with_error

  # validations
  validates :name, presence: true
end
