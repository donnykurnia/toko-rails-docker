FROM ruby:2.3

MAINTAINER Donny Kurnia <donnykurnia@gmail.com>

RUN apt-get update -qq && \
    LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get install -y build-essential libpq-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN useradd --user-group --create-home --shell /bin/false app
ENV HOME=/home/app \
    BUNDLE_APP_CONFIG=/home/app/toko/.bundle \
    BUNDLE_PATH=/home/app/toko/vendor/bundle \
    BUNDLE_BIN=/home/app/toko/vendor/bundle/bin

# COPY . $HOME/toko
RUN mkdir -p $HOME/toko && \
    chown -R app:app $HOME/*

USER app
WORKDIR $HOME/toko

# RUN bundle install

EXPOSE 3000
CMD ["bundle", "exec", "rackup", "-o", "0.0.0.0", "-p", "3000"]
