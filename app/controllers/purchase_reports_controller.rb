class PurchaseReportsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_purchase_item_stocks, only: [:show]

  # GET /purchase_reports
  # GET /purchase_reports.json
  def index
    respond_to do |format|
      format.html
      format.json { render json: PurchaseReportDatatable.new(view_context, date_range: params[:date_range]) }
    end
  end

  # GET /purchase_reports/1
  # GET /purchase_reports/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_purchase_item_stocks
      @purchase_item_stocks = DailyItemStock.includes(:unit, item: :category).references(:unit, item: :category).where(transaction_date: params[:id]).where('total_qty_in > 0')
    end
end
