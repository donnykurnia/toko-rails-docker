class AddUserIdToMainTransaction < ActiveRecord::Migration
  def change
    add_column :main_transactions, :user_id, :uuid
    add_index :main_transactions, :user_id
  end
end
