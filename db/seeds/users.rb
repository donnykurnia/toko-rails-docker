# initial user data
User.find_or_create_by(email: 'admin@toko17.com') do |user|
  user.login = 'admin'
  user.password = 'admin123'
  user.password_confirmation = 'admin123'
end
User.find_or_create_by(email: 'sarah@toko17.com') do |user|
  user.login = 'sarah'
  user.password = 'sarah123'
  user.password_confirmation = 'sarah123'
end
puts 'finished seeding users data'
