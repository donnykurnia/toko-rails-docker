class CreateImportItems < ActiveRecord::Migration
  def change
    create_table :import_items, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.string :autoid
      t.string :name
      t.decimal :buyprice, precision: 15, scale: 2
      t.string :multi_s_pr
      t.decimal :saleprice, precision: 15, scale: 2
      t.string :unit
      t.float :curr_qty
      t.decimal :svalue, precision: 15, scale: 2
      t.decimal :cvalue, precision: 15, scale: 2
      t.string :itemcode
      t.date :createdate
      t.float :re_or_qty
      t.float :order_qty
      t.float :order_tol
      t.string :order_op
      t.string :category
      t.string :subcat1
      t.string :operator
      t.date :lastsaled
      t.decimal :lastinpric, precision: 15, scale: 2
      t.float :lastin_qty

      t.timestamps null: false
    end
  end
end
