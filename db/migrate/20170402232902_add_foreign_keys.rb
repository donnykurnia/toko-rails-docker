class AddForeignKeys < ActiveRecord::Migration
  def change
    add_foreign_key :daily_item_stocks, :items
    add_foreign_key :daily_item_stocks, :units
    add_foreign_key :item_prices, :items
    add_foreign_key :item_prices, :units
    add_foreign_key :item_stocks, :items
    add_foreign_key :item_stocks, :units
    add_foreign_key :main_transactions, :customers
    add_foreign_key :main_transactions, :transaction_types
    add_foreign_key :main_transactions, :users
    add_foreign_key :transaction_items, :main_transactions
    add_foreign_key :transaction_items, :items
    add_foreign_key :transaction_items, :units
    add_foreign_key :transaction_items, :daily_item_stocks
  end
end
