class CreateItemStocks < ActiveRecord::Migration
  def change
    create_table :item_stocks, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.uuid :item_id
      t.uuid :unit_id
      t.integer :qty
      t.integer :position

      t.timestamps null: false
    end
    add_index :item_stocks, :item_id
    add_index :item_stocks, :unit_id
    add_index :item_stocks, :position
    add_index :item_stocks, :created_at
  end
end
