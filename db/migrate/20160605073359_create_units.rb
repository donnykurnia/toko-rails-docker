class CreateUnits < ActiveRecord::Migration
  def change
    create_table :units, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.string :name
      t.boolean :is_base
      t.uuid :base_id
      t.integer :qty

      t.timestamps null: false
    end
    add_index :units, :name
    add_index :units, :is_base
    add_index :units, :base_id
    add_index :units, :created_at
  end
end
