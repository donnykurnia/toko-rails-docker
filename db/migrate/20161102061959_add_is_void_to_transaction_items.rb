class AddIsVoidToTransactionItems < ActiveRecord::Migration
  def change
    add_column :transaction_items, :is_void, :boolean
    add_index :transaction_items, :is_void
    add_column :transaction_items, :daily_item_stock_id, :uuid
    add_index :transaction_items, :daily_item_stock_id
  end
end
