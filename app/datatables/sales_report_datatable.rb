class SalesReportDatatable < AjaxDatatablesRails::Base

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      transaction_date:     { source: 'DailyItemStock.transaction_date' },
      sum_total_qty_out:    { source: 'sum_total_qty_out' },
      sum_total_price_out:  { source: 'sum_total_price_out' },
      sum_total_profit:     { source: 'sum_total_profit' }
    }
  end

  def data
    records.map do |record|
      {
        transaction_date:     link_to(l(record.transaction_date, format: :long), sales_report_path(record.transaction_date.to_s(:db))),
        sum_total_qty_out:    record.sum_total_qty_out.to_f.to_s(:rounded),
        sum_total_price_out:  record.sum_total_price_out.to_f.to_s(:currency),
        sum_total_profit:     record.sum_total_profit.to_f.to_s(:currency),
        show:                 link_to(fa_icon('info-circle'), sales_report_path(record.transaction_date.to_s(:db)), class: 'text-info'),
        DT_RowId:             record.transaction_date.to_s(:db)
      }
    end
  end

  def as_json(options = {})
    {
      draw: params[:draw].to_i,
      recordsTotal:  get_raw_records_non_group.count,
      recordsFiltered: filter_records(get_raw_records_non_group).count,
      data: data,
      total_qty_out: records.reduce(0) { |sum, record| sum + record.sum_total_qty_out }.to_f.to_s(:rounded),
      total_price_out: records.reduce(0) { |sum, record| sum + record.sum_total_price_out }.to_f.to_s(:currency),
      total_profit: records.reduce(0) { |sum, record| sum + record.sum_total_profit }.to_f.to_s(:currency)
    }
  end

  def date_range
    @date_range ||= options[:date_range]
  end

  def from_date
    Date.parse(date_range.split(' - ')[0]) rescue nil if date_range.present?
  end

  def to_date
    Date.parse(date_range.split(' - ')[1]) rescue nil if date_range.present?
  end

  private

  def_delegators :@view, :link_to, :fa_icon, :l, :sales_report_path

  def get_raw_records
    query = DailyItemStock.select("transaction_date, sum(total_qty_out) as sum_total_qty_out, sum(total_price_out) as sum_total_price_out, sum(total_profit) as sum_total_profit").group("transaction_date").having('sum(total_qty_out) > 0')
    query = query.where(transaction_date: from_date..to_date) if from_date.present? && to_date.present?
    query
  end

  def get_raw_records_non_group
    query = DailyItemStock.select("distinct transaction_date").where('total_qty_out > 0')
    query = query.where(transaction_date: from_date..to_date) if from_date.present? && to_date.present?
    query
  end

  # ==== Insert 'presenter'-like methods below if necessary
end
