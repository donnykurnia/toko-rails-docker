# == Schema Information
#
# Table name: item_prices
#
#  id                  :uuid             not null, primary key
#  item_id             :uuid
#  unit_id             :uuid
#  position            :integer
#  min_purchase        :float
#  max_purchase        :float
#  price               :decimal(12, 2)
#  is_base_price       :boolean
#  discount_percentage :float
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  buy_price           :decimal(12, 2)
#  regular_sell_price  :decimal(12, 2)
#
# Indexes
#
#  index_item_prices_on_created_at     (created_at)
#  index_item_prices_on_is_base_price  (is_base_price)
#  index_item_prices_on_item_id        (item_id)
#  index_item_prices_on_position       (position)
#  index_item_prices_on_unit_id        (unit_id)
#
# Foreign Keys
#
#  fk_rails_30c7a965d1  (item_id => items.id)
#  fk_rails_fbc0d4b561  (unit_id => units.id)
#

class ItemPrice < ActiveRecord::Base
  include ActiveSupport::NumberHelper

  # associations
  belongs_to :item, inverse_of: :item_prices
  belongs_to :unit, inverse_of: :item_prices

  # validations
  validates :item, presence: true
  validates :unit, presence: true
  validates :position, numericality: { only_integer: true }, allow_blank: true
  validates :min_purchase, numericality: { greater_than_or_equal_to: 0 }, allow_blank: true
  validates :max_purchase, numericality: { greater_than: 0 }, allow_blank: true
  validates :price, presence: true, numericality: { greater_than: 0 }
  validates :buy_price, presence: true, numericality: { greater_than: 0 }
  validates :regular_sell_price, numericality: { greater_than: 0 }, allow_blank: true
  validates :discount_percentage, numericality: { greater_than_or_equal_to: 0 }, allow_blank: true

  # delegate
  delegate :name, :title, to: :unit, prefix: true, allow_nil: true

  # callback
  after_save :update_daily_item_stock_buy_price

  def amount_range
    if min_purchase.present? && max_purchase.present?
      "#{min_purchase.to_f.to_s(:rounded)}-#{max_purchase.to_f.to_s(:rounded)}: "
    elsif min_purchase.present?
      "&gt; #{min_purchase.to_f.to_s(:rounded)}: ".html_safe
    elsif max_purchase.present?
      "&lt #{max_purchase.to_f.to_s(:rounded)}: ".html_safe
    end
  end

  def title
    "#{amount_range}<strong>#{price.to_f.to_s(:currency)}/#{unit_name}</strong> <small>(#{buy_price.to_f.to_s(:currency)})</small>".html_safe
  end

  protected

  def update_daily_item_stock_buy_price
    today_daily_item_stock = DailyItemStock.today_data(self.item, self.unit).first
    if today_daily_item_stock.present? && self.buy_price.present? && today_daily_item_stock.buy_price != self.buy_price
      today_daily_item_stock.buy_price = self.buy_price
      today_daily_item_stock.total_profit = today_daily_item_stock.total_price_out - (today_daily_item_stock.total_qty_out * today_daily_item_stock.buy_price)
      today_daily_item_stock.save
    end
  end

end
