FactoryGirl.define do
  factory :category do
    sequence(:name) { |n| "kategori #{n}" }
  end
end
