# == Schema Information
#
# Table name: categories
#
#  id         :uuid             not null, primary key
#  parent_id  :uuid
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_categories_on_created_at  (created_at)
#  index_categories_on_name        (name)
#  index_categories_on_parent_id   (parent_id)
#
# Foreign Keys
#
#  fk_rails_82f48f7407  (parent_id => categories.id)
#

class Category < ActiveRecord::Base
  # associations
  belongs_to :parent, class_name: "Category",
                      foreign_key: "parent_id", inverse_of: :sub_categories
  has_many :sub_categories, class_name: "Category",
                            foreign_key: "parent_id", inverse_of: :parent,
                            dependent: :restrict_with_error
  has_many :items, inverse_of: :category, dependent: :restrict_with_error

  # validations
  validates :name, presence: true

  # delegate
  delegate :name, to: :parent, prefix: true, allow_nil: true

  def title
    result = name
    result = "#{parent_name} > #{name}" if parent.present?
    result
  end
end
