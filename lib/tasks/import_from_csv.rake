namespace :import_from_csv do
  desc "Import Categories, Items and ItemPrices data from CSV file"
  task categories_items_and_prices: :environment do
    CSV.foreach(Rails.root.join('db', 'csv', "categories.csv"), {headers: true}) do |row|
      row["name"]
      Category.find_or_create_by(id: row["id"]) do |category|
        category.parent_id  = row["parent_id"]
        category.name       = row["name"]
      end
    end
    CSV.foreach(Rails.root.join('db', 'csv', "units.csv"), {headers: true}) do |row|
      Unit.find_or_create_by(id: row["id"]) do |unit|
        unit.name     = row["name"]
        unit.is_base  = row["is_base"]
        unit.base_id  = row["base_id"]
        unit.qty      = row["qty"]
      end
    end
    CSV.foreach(Rails.root.join('db', 'csv', "items.csv"), {headers: true}) do |row|
      Item.find_or_create_by(id: row["id"]) do |item|
        item.category_id  = row["category_id"]
        item.name         = row["name"]
        item.abbr         = row["abbr"]
        item.autoid       = row["autoid"]
        item.is_active    = row["is_active"] == 't'
      end
    end
    CSV.foreach(Rails.root.join('db', 'csv', "item_prices.csv"), {headers: true}) do |row|
      ItemPrice.find_or_create_by(id: row["id"]) do |item_price|
        item_price.item_id              = row["item_id"]
        item_price.unit_id              = row["unit_id"]
        item_price.position             = row["position"]
        item_price.min_purchase         = row["min_purchase"]
        item_price.max_purchase         = row["max_purchase"]
        item_price.price                = row["price"]
        item_price.is_base_price        = row["is_base_price"]
        item_price.discount_percentage  = row["discount_percentage"]
        item_price.buy_price            = row["buy_price"]
        item_price.regular_sell_price   = row["regular_sell_price"]
      end
    end
  end
end
