# == Schema Information
#
# Table name: daily_item_stocks
#
#  id               :uuid             not null, primary key
#  transaction_date :date
#  item_id          :uuid
#  unit_id          :uuid
#  total_qty_in     :float            default(0.0), not null
#  total_qty_out    :float            default(0.0), not null
#  total_price_in   :decimal(15, 2)   default(0.0), not null
#  total_price_out  :decimal(15, 2)   default(0.0), not null
#  buy_price        :decimal(12, 2)   default(0.0), not null
#  total_profit     :decimal(12, 2)   default(0.0), not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_daily_item_stocks_on_item_id           (item_id)
#  index_daily_item_stocks_on_transaction_date  (transaction_date)
#  index_daily_item_stocks_on_unit_id           (unit_id)
#
# Foreign Keys
#
#  fk_rails_7c44a21139  (unit_id => units.id)
#  fk_rails_a71d6fa901  (item_id => items.id)
#

class DailyItemStock < ActiveRecord::Base
  # associations
  belongs_to :item, inverse_of: :daily_item_stocks
  belongs_to :unit, inverse_of: :daily_item_stocks
  has_many :transaction_items, inverse_of: :daily_item_stock, dependent: :restrict_with_error

  # validations
  validates :transaction_date, presence: true
  validates :item, presence: true
  validates :unit, presence: true

  # delegate
  delegate :name, :category_title, to: :item, prefix: true, allow_nil: true
  delegate :name, :title, to: :unit, prefix: true, allow_nil: true

  def add_new_transaction_item(transaction_item, transaction_type_sign)
    if transaction_type_sign > 0 #beli barang
      self.total_qty_in   += transaction_item.qty
      self.total_price_in += (transaction_item.qty * transaction_item.price)
    else # penjualan
      self.total_qty_out   += transaction_item.qty
      self.total_price_out += (transaction_item.qty * transaction_item.price)
      self.buy_price       = transaction_item.item.item_prices.where(unit: transaction_item.unit).first.try(:buy_price).to_f if self.buy_price == 0
      self.total_profit    = self.total_price_out - (self.total_qty_out * self.buy_price)
    end
    save
  end

  def recalculate(transaction_type_sign)
    the_transaction_items = self.transaction_items.joins(main_transaction: :transaction_type).where('transaction_types.sign = ?', transaction_type_sign)
    if transaction_type_sign > 0 #beli barang
      new_total_qty_in    = 0
      new_total_price_in  = 0
      the_transaction_items.each do |transaction_item|
        new_total_qty_in    += transaction_item.qty
        new_total_price_in  += (transaction_item.qty * transaction_item.price)
      end
      self.update_columns(total_qty_in: new_total_qty_in, total_price_in: new_total_price_in)
    else # penjualan
      new_total_qty_out   = 0
      new_total_price_out = 0
      the_transaction_items.each do |transaction_item|
        new_total_qty_out   += transaction_item.qty
        new_total_price_out += (transaction_item.qty * transaction_item.price)
      end
      new_total_profit  = new_total_price_out - (new_total_qty_out * self.buy_price)
      self.update_columns(total_qty_out: new_total_qty_out, total_price_out: new_total_price_out, total_profit: new_total_profit)
    end
  end

  def self.today_data(item, unit)
    where(item: item, unit: unit, transaction_date: Date.today)
  end

end
