class ChangeDefaultValueInDailyItemStock < ActiveRecord::Migration
  def change
    reversible do |dir|
      dir.up   do
        change_column_default :daily_item_stocks, :total_qty_in, 0
        change_column_default :daily_item_stocks, :total_qty_out, 0
        change_column_default :daily_item_stocks, :total_price_in, 0
        change_column_default :daily_item_stocks, :total_price_out, 0
        change_column_default :daily_item_stocks, :total_qty_in, 0
        change_column_default :daily_item_stocks, :buy_price, 0
        change_column_default :daily_item_stocks, :total_profit, 0
      end
      dir.down do
        change_column_default :daily_item_stocks, :total_qty_in, 0
        change_column_default :daily_item_stocks, :total_qty_out, 0
        change_column_default :daily_item_stocks, :total_price_in, 0
        change_column_default :daily_item_stocks, :total_price_out, 0
        change_column_default :daily_item_stocks, :total_qty_in, 0
        change_column_default :daily_item_stocks, :buy_price, 0
        change_column_default :daily_item_stocks, :total_profit, 0
      end
    end

    change_column_null :daily_item_stocks, :total_qty_in, false
    change_column_null :daily_item_stocks, :total_qty_out, false
    change_column_null :daily_item_stocks, :total_price_in, false
    change_column_null :daily_item_stocks, :total_price_out, false
    change_column_null :daily_item_stocks, :total_qty_in, false
    change_column_null :daily_item_stocks, :buy_price, false
    change_column_null :daily_item_stocks, :total_profit, false
  end
end
