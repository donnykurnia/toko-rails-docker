class CreateItems < ActiveRecord::Migration
  def change
    create_table :items, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.uuid :category_id
      t.string :name
      t.string :abbr

      t.timestamps null: false
    end
    add_index :items, :category_id
    add_index :items, :name
    add_index :items, :abbr
    add_index :items, :created_at
  end
end
