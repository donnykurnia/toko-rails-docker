# == Schema Information
#
# Table name: units
#
#  id         :uuid             not null, primary key
#  name       :string
#  is_base    :boolean
#  base_id    :uuid
#  qty        :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_units_on_base_id     (base_id)
#  index_units_on_created_at  (created_at)
#  index_units_on_is_base     (is_base)
#  index_units_on_name        (name)
#
# Foreign Keys
#
#  fk_rails_ea7ee1469e  (base_id => units.id)
#

class Unit < ActiveRecord::Base
  # associations
  belongs_to :base_unit,  class_name: "Unit",
                          foreign_key: "base_id", inverse_of: :gross_units
  has_many :gross_units,  class_name: "Unit",
                          foreign_key: "base_id", inverse_of: :base_unit,
                          dependent: :restrict_with_error
  # has_many :item_stocks, inverse_of: :unit, dependent: :restrict_with_error
  has_many :item_prices, inverse_of: :unit, dependent: :restrict_with_error
  has_many :transaction_items, inverse_of: :unit, dependent: :restrict_with_error
  has_many :daily_item_stocks, inverse_of: :unit, dependent: :restrict_with_error

  # validations
  validates :name, presence: true
  validates :qty, numericality: { greater_than: 0 }, presence: true

  # delegate
  delegate :name, :title, to: :base_unit, prefix: true, allow_nil: true

  def title
    result = name
    result = "#{name} (#{qty} #{base_unit_name})" if base_unit.present?
    result
  end
end
