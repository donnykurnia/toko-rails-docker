# initial user data
TransactionType.find_or_create_by(name: 'Pembelian') do |transaction_type|
  transaction_type.sign = 1
end
TransactionType.find_or_create_by(name: 'Penjualan') do |transaction_type|
  transaction_type.sign = -1
end
TransactionType.find_or_create_by(name: 'Stok Awal') do |transaction_type|
  transaction_type.sign = 1
end
puts 'finished seeding transaction_type data'
