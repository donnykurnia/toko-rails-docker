# == Schema Information
#
# Table name: main_transactions
#
#  id                  :uuid             not null, primary key
#  number              :string
#  transaction_type_id :uuid
#  total_price         :decimal(15, 2)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  user_id             :uuid
#  transaction_date    :date
#
# Indexes
#
#  index_main_transactions_on_created_at           (created_at)
#  index_main_transactions_on_number               (number)
#  index_main_transactions_on_transaction_date     (transaction_date)
#  index_main_transactions_on_transaction_type_id  (transaction_type_id)
#  index_main_transactions_on_user_id              (user_id)
#
# Foreign Keys
#
#  fk_rails_3b82eda924  (user_id => users.id)
#  fk_rails_59d6d34145  (transaction_type_id => transaction_types.id)
#

class MainTransaction < ActiveRecord::Base
  # associations
  belongs_to :transaction_type, inverse_of: :main_transactions
  has_many :transaction_items, inverse_of: :main_transaction, dependent: :destroy

  accepts_nested_attributes_for :transaction_items, allow_destroy: true

  # validations
  validates :transaction_type, presence: true
  validates :transaction_date, presence: true

  delegate :name, :sign, to: :transaction_type, prefix: true, allow_nil: true

  # callback
  before_create :set_total_price
  after_update  :recalculate_total_price

  def recalculate_total_price
    self.update_columns(total_price: calculate_total_price(false))
  end

  protected

  def calculate_total_price(include_all=true)
    total_price = 0
    self.transaction_items.each do |transaction_item|
      total_price += transaction_item.qty * transaction_item.price if transaction_item.persisted? || include_all
    end
    total_price
  end

  def set_total_price
    self.total_price = calculate_total_price
  end

end
