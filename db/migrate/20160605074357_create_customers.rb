class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.string :name

      t.timestamps null: false
    end
    add_index :customers, :name
    add_index :customers, :created_at
  end
end
