class MainTransactionDatatable < AjaxDatatablesRails::Base

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      transaction_date: { source: 'MainTransaction.transaction_date' },
      transaction_type_name: { source: 'TransactionType.name' },
      total_price: { source: 'MainTransaction.total_price' }
    }
  end

  def data
    records.map do |main_transaction|
      {
        transaction_date:       link_to(l(main_transaction.transaction_date, format: :long), main_transaction),
        transaction_type_name:  main_transaction.transaction_type_name,
        total_price:            content_tag(:p, main_transaction.total_price.to_f.to_s(:currency), class: 'text-right'),
        action:                 action_links(main_transaction),
        DT_RowId:               dom_id(main_transaction)
      }
    end
  end

  private

  def_delegators :@view, :link_to, :fa_icon, :l, :main_transaction_path, :edit_main_transaction_path, :dom_id, :concat, :capture, :content_tag

  def get_raw_records
    MainTransaction.eager_load(:transaction_type).distinct
  end

  # ==== Insert 'presenter'-like methods below if necessary
  def action_links(main_transaction)
    capture do
      concat link_to(fa_icon('info-circle'), main_transaction_path(main_transaction), class: 'text-success')
      concat "&nbsp;".html_safe
      concat link_to(fa_icon('pencil'), edit_main_transaction_path(main_transaction), class: 'text-info')
      concat "&nbsp;".html_safe
      concat link_to(fa_icon('trash'), main_transaction_path(main_transaction), method: :delete, data: {confirm: 'Anda yakin?'}, class: 'text-danger')
    end
  end
end
