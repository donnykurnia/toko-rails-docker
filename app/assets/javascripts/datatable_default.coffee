$.extend $.fn.dataTable.defaults,
  pagingType: 'full_numbers'
  language:
    processing: 'Sedang memproses...'
    search: 'Cari&nbsp;:'
    lengthMenu: 'Tampilkan _MENU_ entri'
    info: 'Menampilkan _START_ sampai _END_ dari _TOTAL_ entri'
    infoEmpty: 'Menampilkan 0 sampai 0 dari 0 entri'
    infoFiltered: '(disaring dari _MAX_ entri keseluruhan)'
    infoPostFix: ''
    loadingRecords: 'Mengambil data...'
    zeroRecords: 'Tidak ditemukan data yang cocok'
    emptyTable: 'Belum ada data'
    paginate:
      first: 'Awal'
      previous: 'Sebelumnya'
      next: 'Selanjutnya'
      last: 'Akhir'
    aria:
      sortAscending: ': aktifkan untuk mengurutkan kolom dari kecil ke besar'
      sortDescending: ': aktifkan untuk mengurutkan kolom dari besar ke kecil'

jQuery ($) ->
  table = $('.datatable').DataTable()
  table.on('draw', ->
    $('#total-qty-out').html(table.ajax.json().total_qty_out);
    $('#total-price-out').html(table.ajax.json().total_price_out);
    $('#total-profit').html(table.ajax.json().total_profit);
    $('#total-qty-in').html(table.ajax.json().total_qty_in);
    $('#total-price-in').html(table.ajax.json().total_price_in);
  )
