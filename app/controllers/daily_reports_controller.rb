class DailyReportsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_daily_item_stocks, only: [:show]

  # GET /daily_reports
  # GET /daily_reports.json
  def index
    respond_to do |format|
      format.html
      format.json { render json: DailyReportDatatable.new(view_context) }
    end
  end

  # GET /daily_reports/1
  # GET /daily_reports/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_daily_item_stocks
      @daily_item_stocks = DailyItemStock.includes(:unit, item: :category).references(:unit, item: :category).where(transaction_date: params[:id])
    end
end
