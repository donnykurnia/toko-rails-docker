class ChangeTransactionDateInMainTransaction < ActiveRecord::Migration
  def change
    remove_column :main_transactions, :transaction_date, :string
    add_column :main_transactions, :transaction_date, :date
    add_index :main_transactions, :transaction_date
  end
end
