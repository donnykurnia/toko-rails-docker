# == Schema Information
#
# Table name: transaction_types
#
#  id         :uuid             not null, primary key
#  name       :string
#  sign       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TransactionType < ActiveRecord::Base
  # associations
  has_many :main_transactions, inverse_of: :transaction_type, dependent: :restrict_with_error

  # validations
  validates :name,  presence: true
  validates :sign,  numericality: { only_integer: true },
                    inclusion: { in: [1, -1], message: "Hanya boleh diisi 1 atau -1" },
                    presence: true
end
