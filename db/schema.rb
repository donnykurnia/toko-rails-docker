# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200927125411) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "uuid-ossp"
  enable_extension "plpgsql"

  create_table "categories", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "parent_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "categories", ["created_at"], name: "index_categories_on_created_at", using: :btree
  add_index "categories", ["name"], name: "index_categories_on_name", using: :btree
  add_index "categories", ["parent_id"], name: "index_categories_on_parent_id", using: :btree

  create_table "customers", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "telephone"
    t.text     "address"
    t.string   "tipe"
  end

  add_index "customers", ["created_at"], name: "index_customers_on_created_at", using: :btree
  add_index "customers", ["name"], name: "index_customers_on_name", using: :btree

  create_table "daily_item_stocks", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.date     "transaction_date"
    t.uuid     "item_id"
    t.uuid     "unit_id"
    t.float    "total_qty_in",                              default: 0.0, null: false
    t.float    "total_qty_out",                             default: 0.0, null: false
    t.decimal  "total_price_in",   precision: 15, scale: 2, default: 0.0, null: false
    t.decimal  "total_price_out",  precision: 15, scale: 2, default: 0.0, null: false
    t.decimal  "buy_price",        precision: 12, scale: 2, default: 0.0, null: false
    t.decimal  "total_profit",     precision: 12, scale: 2, default: 0.0, null: false
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
  end

  add_index "daily_item_stocks", ["item_id"], name: "index_daily_item_stocks_on_item_id", using: :btree
  add_index "daily_item_stocks", ["transaction_date"], name: "index_daily_item_stocks_on_transaction_date", using: :btree
  add_index "daily_item_stocks", ["unit_id"], name: "index_daily_item_stocks_on_unit_id", using: :btree

  create_table "import_items", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "autoid"
    t.string   "name"
    t.decimal  "buyprice",   precision: 15, scale: 2
    t.string   "multi_s_pr"
    t.decimal  "saleprice",  precision: 15, scale: 2
    t.string   "unit"
    t.float    "curr_qty"
    t.decimal  "svalue",     precision: 15, scale: 2
    t.decimal  "cvalue",     precision: 15, scale: 2
    t.string   "itemcode"
    t.date     "createdate"
    t.float    "re_or_qty"
    t.float    "order_qty"
    t.float    "order_tol"
    t.string   "order_op"
    t.string   "category"
    t.string   "subcat1"
    t.string   "operator"
    t.date     "lastsaled"
    t.decimal  "lastinpric", precision: 15, scale: 2
    t.float    "lastin_qty"
    t.datetime "created_at",                          default: "now()", null: false
    t.datetime "updated_at",                          default: "now()", null: false
    t.boolean  "is_active",                           default: true
  end

  add_index "import_items", ["is_active"], name: "index_import_items_on_is_active", using: :btree

  create_table "item_prices", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "item_id"
    t.uuid     "unit_id"
    t.integer  "position"
    t.float    "min_purchase"
    t.float    "max_purchase"
    t.decimal  "price",               precision: 12, scale: 2
    t.boolean  "is_base_price"
    t.float    "discount_percentage"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.decimal  "buy_price",           precision: 12, scale: 2
    t.decimal  "regular_sell_price",  precision: 12, scale: 2
  end

  add_index "item_prices", ["created_at"], name: "index_item_prices_on_created_at", using: :btree
  add_index "item_prices", ["is_base_price"], name: "index_item_prices_on_is_base_price", using: :btree
  add_index "item_prices", ["item_id"], name: "index_item_prices_on_item_id", using: :btree
  add_index "item_prices", ["position"], name: "index_item_prices_on_position", using: :btree
  add_index "item_prices", ["unit_id"], name: "index_item_prices_on_unit_id", using: :btree

  create_table "item_stocks", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "item_id"
    t.uuid     "unit_id"
    t.float    "qty"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "item_stocks", ["created_at"], name: "index_item_stocks_on_created_at", using: :btree
  add_index "item_stocks", ["item_id"], name: "index_item_stocks_on_item_id", using: :btree
  add_index "item_stocks", ["position"], name: "index_item_stocks_on_position", using: :btree
  add_index "item_stocks", ["unit_id"], name: "index_item_stocks_on_unit_id", using: :btree

  create_table "items", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "category_id"
    t.string   "name"
    t.string   "abbr"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "autoid"
    t.boolean  "is_active",   default: true
    t.string   "barcode"
  end

  add_index "items", ["abbr"], name: "index_items_on_abbr", using: :btree
  add_index "items", ["barcode"], name: "index_items_on_barcode", using: :btree
  add_index "items", ["category_id"], name: "index_items_on_category_id", using: :btree
  add_index "items", ["created_at"], name: "index_items_on_created_at", using: :btree
  add_index "items", ["is_active"], name: "index_items_on_is_active", using: :btree
  add_index "items", ["name"], name: "index_items_on_name", using: :btree

  create_table "main_transactions", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "number"
    t.uuid     "transaction_type_id"
    t.decimal  "total_price",         precision: 15, scale: 2
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.uuid     "user_id"
    t.date     "transaction_date"
  end

  add_index "main_transactions", ["created_at"], name: "index_main_transactions_on_created_at", using: :btree
  add_index "main_transactions", ["number"], name: "index_main_transactions_on_number", using: :btree
  add_index "main_transactions", ["transaction_date"], name: "index_main_transactions_on_transaction_date", using: :btree
  add_index "main_transactions", ["transaction_type_id"], name: "index_main_transactions_on_transaction_type_id", using: :btree
  add_index "main_transactions", ["user_id"], name: "index_main_transactions_on_user_id", using: :btree

  create_table "transaction_items", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "main_transaction_id"
    t.uuid     "item_id"
    t.uuid     "unit_id"
    t.float    "qty"
    t.decimal  "price",               precision: 12, scale: 2
    t.decimal  "total_price",         precision: 15, scale: 2
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.boolean  "is_void"
    t.uuid     "daily_item_stock_id"
  end

  add_index "transaction_items", ["created_at"], name: "index_transaction_items_on_created_at", using: :btree
  add_index "transaction_items", ["daily_item_stock_id"], name: "index_transaction_items_on_daily_item_stock_id", using: :btree
  add_index "transaction_items", ["is_void"], name: "index_transaction_items_on_is_void", using: :btree
  add_index "transaction_items", ["item_id"], name: "index_transaction_items_on_item_id", using: :btree
  add_index "transaction_items", ["main_transaction_id"], name: "index_transaction_items_on_main_transaction_id", using: :btree
  add_index "transaction_items", ["unit_id"], name: "index_transaction_items_on_unit_id", using: :btree

  create_table "transaction_types", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.integer  "sign"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "units", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.boolean  "is_base"
    t.uuid     "base_id"
    t.integer  "qty"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "units", ["base_id"], name: "index_units_on_base_id", using: :btree
  add_index "units", ["created_at"], name: "index_units_on_created_at", using: :btree
  add_index "units", ["is_base"], name: "index_units_on_is_base", using: :btree
  add_index "units", ["name"], name: "index_units_on_name", using: :btree

  create_table "users", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "login",                  default: "", null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["created_at"], name: "index_users_on_created_at", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["login"], name: "index_users_on_login", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "categories", "categories", column: "parent_id"
  add_foreign_key "daily_item_stocks", "items"
  add_foreign_key "daily_item_stocks", "units"
  add_foreign_key "item_prices", "items"
  add_foreign_key "item_prices", "units"
  add_foreign_key "item_stocks", "items"
  add_foreign_key "item_stocks", "units"
  add_foreign_key "items", "categories"
  add_foreign_key "main_transactions", "transaction_types"
  add_foreign_key "main_transactions", "users"
  add_foreign_key "transaction_items", "daily_item_stocks"
  add_foreign_key "transaction_items", "items"
  add_foreign_key "transaction_items", "main_transactions"
  add_foreign_key "transaction_items", "units"
  add_foreign_key "units", "units", column: "base_id"
end
