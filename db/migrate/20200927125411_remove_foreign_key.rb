class RemoveForeignKey < ActiveRecord::Migration
  def change
    remove_reference :main_transactions, :customer, type: :uuid, index: true, foreign_key: true
  end
end
