#!/bin/sh

TIMESTAMP=`date +%Y%m%d%H%M%S`

echo 'backup started!'

/usr/bin/pg_dump -c --if-exists -f ${BACKUP_PATH}/toko_rails_${TIMESTAMP}_development.sql

echo "${BACKUP_PATH}/toko_rails_${TIMESTAMP}_development.sql saved!"

echo 'backup finished!'
