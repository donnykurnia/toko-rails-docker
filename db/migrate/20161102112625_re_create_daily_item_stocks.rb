class ReCreateDailyItemStocks < ActiveRecord::Migration
  def change
    drop_table :daily_item_stocks do |t|
      t.date :transaction_date
      t.uuid :item_id
      t.uuid :unit_id
      t.float :total_qty_in
      t.float :total_qty_out
      t.decimal :total_price_in, precision: 15, scale: 2
      t.decimal :total_price_out, precision: 15, scale: 2
      t.decimal :total_profit, precision: 12, scale: 2

      t.timestamps null: false
    end

    create_table :daily_item_stocks, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.date :transaction_date
      t.uuid :item_id
      t.uuid :unit_id
      t.float :total_qty_in
      t.float :total_qty_out
      t.decimal :total_price_in, precision: 15, scale: 2
      t.decimal :total_price_out, precision: 15, scale: 2
      t.decimal :buy_price, precision: 12, scale: 2
      t.decimal :total_profit, precision: 12, scale: 2

      t.timestamps null: false
    end
    add_index :daily_item_stocks, :transaction_date
    add_index :daily_item_stocks, :item_id
    add_index :daily_item_stocks, :unit_id
  end
end
