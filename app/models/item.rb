# == Schema Information
#
# Table name: items
#
#  id          :uuid             not null, primary key
#  category_id :uuid
#  name        :string
#  abbr        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  autoid      :string
#  is_active   :boolean          default(TRUE)
#  barcode     :string
#
# Indexes
#
#  index_items_on_abbr         (abbr)
#  index_items_on_barcode      (barcode)
#  index_items_on_category_id  (category_id)
#  index_items_on_created_at   (created_at)
#  index_items_on_is_active    (is_active)
#  index_items_on_name         (name)
#
# Foreign Keys
#
#  fk_rails_89fb86dc8b  (category_id => categories.id)
#

class Item < ActiveRecord::Base
  # associations
  belongs_to :category, inverse_of: :items
  # has_many :item_stocks, inverse_of: :item, dependent: :restrict_with_error
  has_many :item_prices, -> { order("position asc") },
    inverse_of: :item, dependent: :restrict_with_error
  has_many :transaction_items, inverse_of: :item, dependent: :restrict_with_error
  has_many :daily_item_stocks, inverse_of: :item, dependent: :restrict_with_error

  accepts_nested_attributes_for :item_prices, allow_destroy: true

  # validations
  validates :name, presence: true
  validates :category, presence: true

  # delegate
  delegate :name, :title, to: :category, prefix: true, allow_nil: true

  def title
    name
  end

  def to_s
    title
  end

  def ordered_item_prices
    item_prices.sort_by do |item_price|
      item_price.position
    end
  end

  def self.select2(query)
    Item.includes(item_prices: :unit).where('("items"."name" ILIKE :query)', { query: "%#{sanitize_sql_like(query.downcase)}%" })
  end
end
