#!/bin/bash

mkdir -p vendor/bundle/ruby/2.3.0
bundle install
bundle exec rails new . --force --database=postgresql
