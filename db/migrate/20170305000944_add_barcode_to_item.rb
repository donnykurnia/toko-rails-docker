class AddBarcodeToItem < ActiveRecord::Migration
  def change
    add_column :items, :barcode, :string
    add_index :items, :barcode
  end
end
