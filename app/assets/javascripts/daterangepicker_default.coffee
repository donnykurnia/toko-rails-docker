jQuery ($) ->
  $('input.daterange').daterangepicker
    alwaysShowCalendars: true
    locale:
      applyLabel: 'Pilih'
      cancelLabel: 'Batal'
      format: 'YYYY/MM/DD'
      customRangeLabel: 'Pilihan kalender'
    ranges:
      'Hari ini': [
        moment()
        moment()
      ]
      'Kemarin': [
        moment().subtract(1, 'days')
        moment().subtract(1, 'days')
      ]
      '7 hari yang lalu': [
        moment().subtract(6, 'days')
        moment()
      ]
      '30 hari yang lalu': [
        moment().subtract(29, 'days')
        moment()
      ]
      'Bulan ini': [
        moment().startOf('month')
        moment().endOf('month')
      ]
      'Bulan lalu': [
        moment().subtract(1, 'month').startOf('month')
        moment().subtract(1, 'month').endOf('month')
      ]
    showDropdowns: true
