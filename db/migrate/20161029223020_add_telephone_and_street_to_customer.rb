class AddTelephoneAndStreetToCustomer < ActiveRecord::Migration
  def change
    add_column :customers, :telephone, :string
    add_column :customers, :address, :text
  end
end
