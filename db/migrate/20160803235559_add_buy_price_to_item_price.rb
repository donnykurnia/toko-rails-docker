class AddBuyPriceToItemPrice < ActiveRecord::Migration
  def change
    add_column :item_prices, :buy_price, :decimal, precision: 12, scale: 2
  end
end
