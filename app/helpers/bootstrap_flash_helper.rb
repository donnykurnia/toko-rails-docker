module BootstrapFlashHelper
  include ActionView::Helpers::OutputSafetyHelper
  include ActionView::Helpers::TagHelper

  ALERT_TYPES = [:danger, :info, :success, :warning] unless const_defined?(:ALERT_TYPES)

  def bootstrap_flash(display_type='page')
    flash_messages = []
    flash.each do |type, message|
      # Skip empty messages, e.g. for devise messages set to nothing in a locale file.
      next if message.blank?

      type = type.to_sym
      type = :success if type == :notice
      type = :danger  if type == :alert
      next unless ALERT_TYPES.include?(type)

      message = parse_message(message)
      if message.kind_of?(String)
        if display_type == 'api'
          flash_messages << build_message_text_for_api(message, type)
        else
          flash_messages << build_message_text(message, type)
        end
      elsif message.kind_of?(Array)
        message.each do |msg|
          if display_type == 'api'
            flash_messages << build_message_text_for_api(msg, type) if msg
          else
            flash_messages << build_message_text(msg, type) if msg
          end
        end
      end
    end
    if display_type == 'api'
      flash_messages
    else
      flash_messages.join("\n").html_safe
    end
  end

private

  def parse_message(message)
    begin
      JSON.parse(message)
    rescue JSON::ParserError, TypeError
      message
    end
  end

  def build_message_text(msg, type)
    content_tag(:div, content_tag(:button, raw("&times;"), class: "close", "data-dismiss" => "alert") + msg, class: "alert fade in alert-#{type}")
  end

  def build_message_text_for_api(msg, type)
    {type: type, message: msg}
  end
end
