# == Schema Information
#
# Table name: item_stocks
#
#  id         :uuid             not null, primary key
#  item_id    :uuid
#  unit_id    :uuid
#  qty        :float
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_item_stocks_on_created_at  (created_at)
#  index_item_stocks_on_item_id     (item_id)
#  index_item_stocks_on_position    (position)
#  index_item_stocks_on_unit_id     (unit_id)
#
# Foreign Keys
#
#  fk_rails_95760101ae  (item_id => items.id)
#  fk_rails_9e626f645f  (unit_id => units.id)
#

class ItemStock < ActiveRecord::Base
  # associations
  belongs_to :item, inverse_of: :item_stocks
  belongs_to :unit, inverse_of: :item_stocks

  # validations
  validates :item, presence: true
  validates :unit, presence: true
  validates :qty, numericality: { greater_than: 0 }, presence: true
  validates :position, numericality: { only_integer: true }, allow_blank: true

  delegate :name, :title, to: :unit, prefix: true, allow_nil: true

  def title
    "#{qty} #{unit_name}"
  end
end
