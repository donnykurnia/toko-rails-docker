class ChangeQtyTypeInItemStock < ActiveRecord::Migration
  def change
    reversible do |dir|
      change_table :item_stocks do |t|
        dir.up   { t.change :qty, :float }
        dir.down { t.change :qty, :integer }
      end
      change_table :transaction_items do |t|
        dir.up   { t.change :qty, :float }
        dir.down { t.change :qty, :integer }
      end
      change_table :item_prices do |t|
        dir.up   { t.change :min_purchase, :float }
        dir.down { t.change :min_purchase, :integer }
        dir.up   { t.change :max_purchase, :float }
        dir.down { t.change :max_purchase, :integer }
      end
    end
  end
end
