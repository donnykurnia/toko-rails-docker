class CreateMainTransactions < ActiveRecord::Migration
  def change
    create_table :main_transactions, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.string :number
      t.string :transaction_date
      t.uuid :customer_id
      t.uuid :transaction_type_id
      t.decimal :total_price, precision: 15, scale: 2

      t.timestamps null: false
    end
    add_index :main_transactions, :number
    add_index :main_transactions, :transaction_date
    add_index :main_transactions, :customer_id
    add_index :main_transactions, :transaction_type_id
    add_index :main_transactions, :created_at
  end
end
