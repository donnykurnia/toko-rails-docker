# == Schema Information
#
# Table name: transaction_items
#
#  id                  :uuid             not null, primary key
#  main_transaction_id :uuid
#  item_id             :uuid
#  unit_id             :uuid
#  qty                 :float
#  price               :decimal(12, 2)
#  total_price         :decimal(15, 2)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  is_void             :boolean
#  daily_item_stock_id :uuid
#
# Indexes
#
#  index_transaction_items_on_created_at           (created_at)
#  index_transaction_items_on_daily_item_stock_id  (daily_item_stock_id)
#  index_transaction_items_on_is_void              (is_void)
#  index_transaction_items_on_item_id              (item_id)
#  index_transaction_items_on_main_transaction_id  (main_transaction_id)
#  index_transaction_items_on_unit_id              (unit_id)
#
# Foreign Keys
#
#  fk_rails_3d20500557  (main_transaction_id => main_transactions.id)
#  fk_rails_58ab03b355  (item_id => items.id)
#  fk_rails_72384ea74e  (unit_id => units.id)
#  fk_rails_98ccc28e58  (daily_item_stock_id => daily_item_stocks.id)
#

class TransactionItem < ActiveRecord::Base
  # associations
  belongs_to :main_transaction, inverse_of: :transaction_items
  belongs_to :item, inverse_of: :transaction_items
  belongs_to :unit, inverse_of: :transaction_items
  belongs_to :daily_item_stock, inverse_of: :transaction_items

  # validation
  validates :main_transaction, presence: true
  validates :item, presence: true
  validates :unit, presence: true
  validates :qty, numericality: { greater_than: 0 }, presence: true
  validates :price, numericality: { greater_than: 0 }, presence: true

  # delegate
  delegate :name, :title, :name, to: :item, prefix: true, allow_nil: true
  delegate :name, :title, to: :unit, prefix: true, allow_nil: true

  # callback
  before_save :calculate_total_price
  after_create :add_to_daily_item_stock
  after_update :update_daily_item_stock
  after_destroy :remove_from_daily_item_stock

  protected

  def calculate_total_price
    self.total_price = self.qty * self.price
  end

  def add_to_daily_item_stock
    daily_item_stock = DailyItemStock.find_or_create_by(transaction_date: main_transaction.transaction_date, item: item, unit: unit)
    daily_item_stock.add_new_transaction_item(self, main_transaction.transaction_type_sign)
    self.update_columns(daily_item_stock_id: daily_item_stock.id)
  end

  def update_daily_item_stock
    if changes.include?(:item_id) || changes.include?(:unit_id)
      # handle when item and/or unit changed
      old_daily_item_stock = DailyItemStock.find_or_create_by(transaction_date: main_transaction.transaction_date, item_id: item_id_was, unit_id: unit_id_was)
      new_daily_item_stock = DailyItemStock.find_or_create_by(transaction_date: main_transaction.transaction_date, item_id: item_id, unit_id: unit_id)
      # update daily_item_stock
      self.update_columns(daily_item_stock_id: new_daily_item_stock.id)
      # recalculate both old and new daily_item_stock
      old_daily_item_stock.recalculate(main_transaction.transaction_type_sign)
      new_daily_item_stock.recalculate(main_transaction.transaction_type_sign)
    else
      # only qty changed
      daily_item_stock.recalculate(main_transaction.transaction_type_sign)
    end
  end

  def remove_from_daily_item_stock
    daily_item_stock.recalculate(main_transaction.transaction_type_sign)
  end

end
