class AddAutoidAndIsActiveToItem < ActiveRecord::Migration
  def change
    add_column :items, :autoid, :string
    add_column :items, :is_active, :boolean, default: true
    add_index :items, :is_active
  end
end
