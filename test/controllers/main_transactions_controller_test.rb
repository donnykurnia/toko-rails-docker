require 'test_helper'

class MainTransactionsControllerTest < ActionController::TestCase
  setup do
    @main_transaction = main_transactions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:main_transactions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create main_transaction" do
    assert_difference('MainTransaction.count') do
      post :create, main_transaction: { customer_id: @main_transaction.customer_id, number: @main_transaction.number, transaction_date: @main_transaction.transaction_date, transaction_type_id: @main_transaction.transaction_type_id }
    end

    assert_redirected_to main_transaction_path(assigns(:main_transaction))
  end

  test "should show main_transaction" do
    get :show, id: @main_transaction
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @main_transaction
    assert_response :success
  end

  test "should update main_transaction" do
    patch :update, id: @main_transaction, main_transaction: { customer_id: @main_transaction.customer_id, number: @main_transaction.number, transaction_date: @main_transaction.transaction_date, transaction_type_id: @main_transaction.transaction_type_id }
    assert_redirected_to main_transaction_path(assigns(:main_transaction))
  end

  test "should destroy main_transaction" do
    assert_difference('MainTransaction.count', -1) do
      delete :destroy, id: @main_transaction
    end

    assert_redirected_to main_transactions_path
  end
end
