# == Schema Information
#
# Table name: users
#
#  id                     :uuid             not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  login                  :string           default(""), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_created_at            (created_at)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_login                 (login) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # validates :login, presence: true,
  #   uniqueness: {
  #     case_sensitive: false
  #   }
  # validates_format_of :login, with: /^[a-zA-Z0-9_\.]*$/, multiline: true
  # validate :validate_login

  # protected
  #   def validate_login
  #     if User.where(email: login).exists?
  #       errors.add(:login, :invalid)
  #     end
  #   end

  #   def self.find_for_database_authentication(conditions)
  #     local_conditions = conditions.dup
  #     if login = conditions.delete(:login)
  #       where(local_conditions.to_h).where(["lower(login) = :value", { :value => login.downcase }]).first
  #     elsif local_conditions.has_key?(:email)
  #       find_for_authentication(local_conditions)
  #     end
  #   end
end
