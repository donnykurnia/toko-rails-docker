module ApplicationHelper
  def title(title_text, website_title_position='back')
    content_for :title do
      concat t('layouts.application.title') + " | " if website_title_position == 'front'
      concat title_text
      concat " | " + t('layouts.application.title') if website_title_position == 'back'
    end
  end
end
