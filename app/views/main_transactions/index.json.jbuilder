json.array!(@main_transactions) do |main_transaction|
  json.extract! main_transaction, :id, :transaction_date, :transaction_type_id
  json.url main_transaction_url(main_transaction, format: :json)
end
