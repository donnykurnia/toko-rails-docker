class CreateItemPrices < ActiveRecord::Migration
  def change
    create_table :item_prices, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.uuid :item_id
      t.uuid :unit_id
      t.integer :position
      t.integer :min_purchase
      t.integer :max_purchase
      t.decimal :price, precision: 12, scale: 2
      t.boolean :is_base_price
      t.float :discount_percentage

      t.timestamps null: false
    end
    add_index :item_prices, :item_id
    add_index :item_prices, :unit_id
    add_index :item_prices, :position
    add_index :item_prices, :is_base_price
    add_index :item_prices, :created_at
  end
end
