class CreateTransactionItems < ActiveRecord::Migration
  def change
    create_table :transaction_items, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.uuid :main_transaction_id
      t.uuid :item_id
      t.uuid :unit_id
      t.integer :qty
      t.decimal :price, precision: 12, scale: 2
      t.decimal :total_price, precision: 15, scale: 2

      t.timestamps null: false
    end
    add_index :transaction_items, :main_transaction_id
    add_index :transaction_items, :item_id
    add_index :transaction_items, :unit_id
    add_index :transaction_items, :created_at
  end
end
