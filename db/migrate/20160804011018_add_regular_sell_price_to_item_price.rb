class AddRegularSellPriceToItemPrice < ActiveRecord::Migration
  def change
    add_column :item_prices, :regular_sell_price, :decimal, precision: 12, scale: 2
  end
end
