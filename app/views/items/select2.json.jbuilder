json.total_count @items.count
json.items @items.page(params[:page]).per(params[:per_page] || 30) do |item|
  json.(item, :id, :category_id, :name, :title, :is_active, :autoid)
  json.item_prices item.ordered_item_prices do |item_price|
    json.(item_price, :id, :item_id, :unit_id, :position, :min_purchase, :max_purchase, :price, :is_base_price, :discount_percentage, :buy_price, :regular_sell_price, :title)
  end
end
