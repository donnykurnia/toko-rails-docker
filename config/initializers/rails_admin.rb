RailsAdmin.config do |config|

  config.main_app_name = Proc.new { |controller| [ "Toko17", "Admin - #{controller.params[:action].try(:titleize)}" ] }

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.label_methods = [:title, :name]

  config.model 'Category' do
    list do
      field :parent
      sort_by :name
      field :name do
        sort_reverse false
      end
      field :sub_categories
      field :items
    end
  end
  config.model 'Customer' do
    list do
      sort_by :name
      field :name do
        sort_reverse false
      end
      field :tipe
      field :telephone
      field :address
    end
    edit do
      exclude_fields :main_transactions
    end
  end
  config.model 'Item' do
    list do
      sort_by :name
      field :name do
        sort_reverse false
      end
      field :abbr
      field :item_prices
      field :category
      field :is_active
    end
    edit do
      exclude_fields :transaction_items, :daily_item_stocks, :autoid
    end
  end
  config.model 'TransactionType' do
    list do
      sort_by :name
      field :name do
        sort_reverse false
      end
      field :sign
    end
    edit do
      exclude_fields :main_transactions
    end
  end
  config.model 'Unit' do
    list do
      sort_by :name
      field :name do
        sort_reverse false
      end
      field :is_base
      field :base_unit
      field :qty
      field :gross_units
      field :item_prices
    end
    edit do
      exclude_fields :transaction_items, :daily_item_stocks
    end
  end
  # hidden models
  config.model 'DailyItemStock' do
    visible false
  end
  config.model 'ImportItem' do
    visible false
  end
  config.model 'ItemPrice' do
    visible false
  end
  config.model 'ItemStock' do
    visible false
  end
  config.model 'TransactionItem' do
    visible false
  end
end
